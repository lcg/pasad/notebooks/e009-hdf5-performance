import click
import h5py as h5
import itertools as it
import numpy as np
import os
import pandas as pd
import plx
import traceback
from enum import Enum
from functools import wraps
from multiprocessing.pool import Pool
from tempfile import mkstemp
from time import time
from typing import List

spike_source_dtype = [('time', 'u8'), ('unit', 'i2'), ('wave', '(32,)i2'), ('padding', 'S6')]


class DataType(Enum):
    SLOW = {'source': 'i2', 'target': 'f4'}
    SPIKE = {
        'source': spike_source_dtype,
        'target': [('time', 'u8'), ('unit', 'u1'), ('wave', '(32,)f4')]
    }
    SPIKE_TIME = {
        'source': spike_source_dtype,
        'target': ('time', 'u8')
    }
    SPIKE_WAVE = {
        'source': spike_source_dtype,
        'target': ('wave', 'f4')
    }
    SPIKE_UNIT = {
        'source': spike_source_dtype,
        'target': ('unit', 'u1')
    }


@click.command()
@click.argument('data_path', type=click.Path(exists=True, readable=True, dir_okay=False))
@click.argument('output_path', type=click.Path(writable=True, dir_okay=False))
@click.option('--dtype', '-d', type=click.Choice([str(key.name) for key in DataType]), required=True)
@click.option('--rounds', '-n', type=int, default=10)
def main(data_path, output_path, dtype, rounds):
    df = compare_parameters(
        data_path=data_path,
        rounds=rounds,
        dtype=DataType[dtype],
    )
    df.to_csv(output_path)


def compare_parameters(data_path: str, rounds: int, dtype: DataType) -> pd.DataFrame:
    arguments = (
        (data_path, options, rounds, dtype)
        for options in parameters(dtype)
    )

    with Pool(max(1, os.cpu_count() - 1)) as pool:
        rows = pool.starmap(evaluate_options, arguments)
    
    return pd.DataFrame(rows)


def evaluate_options(data_path:str, options: dict, rounds: int, dtype: DataType) -> dict:
    written_tempfiles = []
    evaluation = options.copy()
    evaluation['compression_opts'] = str(evaluation['compression_opts'])
    data = np.memmap(data_path, mode="r", dtype=dtype.value['source'])

    @timeit(rounds)
    def test_read():
        tempfile = next(read_tempfiles)
        with h5.File(tempfile, 'r') as h5file:
            array = np.empty_like(h5file['data'])
            array[:] = h5file['data']

    @timeit(rounds)
    def test_write():
        _, tempfile = mkstemp(suffix=f'-{__name__}.h5')
        with h5.File(tempfile, 'w') as h5file:
            written_tempfiles.append(tempfile)

            if dtype in [DataType.SLOW, DataType.SPIKE]:
                dataset = h5file.create_dataset('data', data.shape, dtype=dtype.value['target'], **options)
                dataset[:] = data
            elif dtype in [DataType.SPIKE_TIME, DataType.SPIKE_UNIT]:
                field_name, field_dtype = dtype.value['target']
                dataset = h5file.create_dataset('data', data[field_name].shape, dtype=field_dtype, **options)
                dataset[:] = data[field_name]
            elif dtype in [DataType.SPIKE_WAVE]:
                field_name, field_dtype = dtype.value['target']
                dataset = h5file.create_dataset('data', (len(data[field_name]), 32), dtype=field_dtype, **options)
                dataset[:] = data[field_name]

    write_time = test_write()
    read_tempfiles = iter(written_tempfiles)
    read_time = test_read()

    evaluation['size'] = file_size(written_tempfiles[0])
    evaluation['size_factor'] = evaluation['size'] / file_size(data_path)
    evaluation['write_time'] = write_time
    evaluation['read_time'] = read_time

    for tempfile in written_tempfiles:
        os.unlink(tempfile)

    return evaluation


def file_size(fname: str) -> int:
    return os.stat(fname).st_size


def parameters(dtype: DataType) -> List[dict]:
    def add_params(**more_params):
        params = {**base_params, **more_params}
        parameters.append(params)

    parameters = []

    for shuffle in [False, True]:
        for fletcher32 in [False, True]:
            base_params = {
                "chunks": True,
                "fletcher32": fletcher32,
                "shuffle": shuffle,
            }

            add_params(compression='lzf', compression_opts=None)
            
            for level in range(10):
                add_params(compression='gzip', compression_opts=level)

            if dtype in [DataType.SLOW]:
                for ecnn in ['ec', 'nn']:
                    for level in range(2, 33, 2):
                        add_params(compression='szip', compression_opts=(ecnn, level))

    return parameters


def timeit(rounds: int):
    def wrapper(func):
        @wraps(func)
        def timed(*args, **kwargs):
            a = time()
            func(*args, **kwargs)
            b = time()
            return b - a

        @wraps(func)
        def averaged(*args, **kwargs):
            return sum(timed(*args, **kwargs) for i in range(rounds)) / rounds
            
        return averaged

    return wrapper


if __name__ == '__main__':
    main()
