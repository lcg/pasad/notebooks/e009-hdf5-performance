import plx
from pathlib import Path
from sys import argv

recording_name=argv[1]
data_directory = "data"
export_dir = f"{data_directory}/{recording_name}"
plx_path = f"{data_directory}/{recording_name}.plx"

Path(export_dir).mkdir(parents=True, exist_ok=True)
plx.PlxFile(plx_path, export_base_dir=export_dir).export_file()

