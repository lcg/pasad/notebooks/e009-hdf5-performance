E009 &ndash; HDF5 storage performance compared to raw arrays
============================================================

```bash
python3.7 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
rclone copy lcg-drive:Neuroscience/Datasets/ProjectV2/data/V206/Dados/v206/v206-rf-0.plx data
dvc repro
```
